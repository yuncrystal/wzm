import React, { useRef, useEffect, useState } from 'react';
// import { Player, ControlBar } from 'video-react';
import ReactPlayer from 'react-player'
import './App.css';

function App() {
  const video = useRef(null);
  const videoBackground = useRef(null);
  return (
    <div>
      <div className='pc-banner'>
        <img src={process.env.PUBLIC_URL + '/imgs/pc-banner.png'} alt="pc-banner" />
      </div>
      <div className='app'>
        <div className="container">
          <div className='banner-title'>
            <div className='red h2 semi-bold ls-50'>全澳大利亚销量第一</div>
            <div className='red h2 semi-bold ls-100'>@佟大为同款养胃粉</div>
            <div className='orange2 orange-font semi-bold'>#胃靠七分养 吃货尝天下#</div>
          </div>
          <div className='padding background-two-color'>
            <img className='banner-img' src={process.env.PUBLIC_URL + '/imgs/tong-weibo.png'} alt="tong-weibo" />
          </div>
          <div className='section-weibo-content'>
            <div className='h7 grey'>完整养胃视频请上微博搜索<br></br> #胃靠七分养 吃货尝天下#或@佟大为</div>
            <div className='row-two'>
              <div >
                <img src={process.env.PUBLIC_URL + '/imgs/IRI-logo.png'} alt="IRI-logo" />
              </div>
              <div className='h7 black'>澳洲权威数据统计机构数据所得，纽乐养胃粉全澳销量第一</div>
              <div className='product-container'>
                <img src={process.env.PUBLIC_URL + '/imgs/product1.png'} alt="product1" />
              </div>
            </div>
          </div>

          <div className='h4 content1 black'>
            感谢著名中国实力派演员—佟大为对Nutra-Life 纽乐养胃粉的支持和信任，为广大吃货宝宝们带来走心的亲测推荐。纽乐养胃粉不积食，不涨气，不闹肚子，不爆痘，关键还很好喝。即日起到7月31日，参与我们活动活动便有机会赢得丰富礼品：
          </div>

          <div className='active-1'>
            <div className='row1'>
              <div>
                <img src={process.env.PUBLIC_URL + '/imgs/active1.png'} alt="active1.1.png" />
              </div>
              <div className='content black'>上微博关注@纽乐Nutra-life转发并评论活动微博，转发量前10名将获得：</div>
            </div>
            <div className='row2'>
              <div className='font20 red bold'>佟大为同款养胃粉</div>
              <div>
                <img src={process.env.PUBLIC_URL + '/imgs/active1.1.png'} alt="active1.1.png" />
              </div>
            </div>
            <div className='row3'>
              <div>
                <img src={process.env.PUBLIC_URL + '/imgs/active1.2.png'} alt="active1.2.png" />
              </div>
              <div className='font20 red bold'>
                佟大为签名照
              </div>

            </div>
            <div className='row4'>
              <div className='font20 red bold'>
                咕咕鸡保温杯
              </div>
              <div>
                <img src={process.env.PUBLIC_URL + '/imgs/active1.3.png'} alt="active1.3.png" />
              </div>
            </div>
            <div className='row5'>
              <div className='h4 black med-weight ls-200'>
                活动平台：
              </div>
              <div>
                <img src={process.env.PUBLIC_URL + '/imgs/weibo.png'} alt="weibo-logo" />
              </div>

            </div>

          </div>
         

          <div className='active-1 box' style={{marginTop:38,paddingBottom:18}}>
            <div className='row1'>
              <div>
                <img src={process.env.PUBLIC_URL + '/imgs/active2.png'} alt="active2.png" />
              </div>
              <div className='content black h4'>添加活动助手：获取微信有奖活动详情</div>
            </div>
            <div className='row6'>
              <img src={process.env.PUBLIC_URL + '/imgs/QR2.png'} alt="QR" />
            </div>
            <div className='row5'>
              <div className='h4 black med-weight ls-200'>
                活动平台：
              </div>
              <div >
                <img src={process.env.PUBLIC_URL + '/imgs/wechat.png'} alt="weichat-logo" />
              </div>
              <div style={{paddingLeft:6}} className='black med-weight'>
                Wechat
              </div>

            </div>

          </div>
          <div className='banner'>
            <div className='h7' style={{textAlign:'center',paddingTop:13}}>*纽乐Nutra-Life 拥有以上活动最终解释权</div>
            <img className='banner-img' src={process.env.PUBLIC_URL + '/imgs/banner1.png'} alt="Gut Relif" />

            <div className='banner-group'>
              <div className='row-1'>
                <img className='group-1' src={process.env.PUBLIC_URL + '/imgs/group-1.png'} alt="group-1.png" />
                <img className='group-2' src={process.env.PUBLIC_URL + '/imgs/group-2.png'} alt="group-2.png" />
                <img className='group-3' src={process.env.PUBLIC_URL + '/imgs/group-3.png'} alt="group-3.png" />
              </div>
              <div className='row-2'>
                <img className='group-4' src={process.env.PUBLIC_URL + '/imgs/group-4.png'} alt="group-4.png" />
                <img className='group-5' src={process.env.PUBLIC_URL + '/imgs/group-5.png'} alt="group-5.png" />
              </div>
            </div>
          </div>

          <div className='label-right-container'>
            <div className='label-right'>
              澳洲注册药剂师<br></br>共同倾情推荐
        </div>
          </div>

          <div className='IRI-logo padding'>
            <img src={process.env.PUBLIC_URL + '/imgs/IRI-logo.png'} alt="IRI-logo" />
            <div className='h7 black'>澳洲权威数据统计机构数据所得，纽乐养胃粉全澳销量No.1</div>
          </div>

          <div className='product-effect'>
            <div className='label-left'>
              <div className='label-left-content h3'>产品功效</div>
            </div>
            <div className='effect-list padding'>
              <div className='effect-row'>
                <img src={process.env.PUBLIC_URL + '/imgs/effect-1.svg'} alt="" />
                <div>维护健康的消化功能，保养肝脏。</div>
              </div>
              <div className='effect-row'>
                <img src={process.env.PUBLIC_URL + '/imgs/effect-2.svg'} alt="" />
                <div>缓解消化不良和肠胃气胀，帮助舒缓受到胃肠道 刺激的肠壁黏膜。</div>
              </div>
              <div className='effect-row'>
                <img src={process.env.PUBLIC_URL + '/imgs/effect-3.svg'} alt="" />
                <div>含有益生元，促进肠道内有益菌群健康生长， 缓解便秘。</div>
              </div>
              <div className='effect-row'>
                <img src={process.env.PUBLIC_URL + '/imgs/effect-4.svg'} alt="" />
                <div>好喝的芒果香橙味</div>
              </div>

            </div>

          </div>

          <div className='tips'>
            <div className='label-left-tip'>
              <div className='label-left-content h3'>使用小贴士</div>
            </div>

            <div className='tips-list padding'>
              <div className='tip-row'>
                <div className='orange font20'>01</div>
                <div className='tip-content'>
                  <div className='bold'>建议冷水或温水冲泡，水温不宜过高</div>
                  <div>以保留养胃粉中的益生元活性及营养成分。</div>
                </div>
              </div>

              <div className='tip-row'>
                <div className='orange font20'>02</div>
                <div className='tip-content'>
                  <div className='bold'>内附量勺，每次使用一平勺 </div>
                  <div>一勺6克兑150ml冷水或温水搅均匀即可饮用。</div>
                </div>
              </div>

              <div className='tip-row'>
                <div className='orange font20'>03</div>
                <div className='tip-content'>
                  <div className='bold'>每日一次，或遵医嘱 </div>
                  <div>服用后请大量饮水。</div>
                </div>
              </div>
            </div>
          </div>

          <div className='about-left-tip'>
            <div className='label-left-content h3'>关于纽乐Nutra-Life</div>
          </div>
          <div className='video-container' >
            <div ref={videoBackground} className='video-background' onClick={(e) => {
              video.current.play();
              video.current.muted = false;
              video.current.style.display = 'block';
              videoBackground.current.style.zIndex = -1;
            }}>
              <img style={{ width: '100%', height: '100%' }} src={process.env.PUBLIC_URL + '/imgs/background.png'} alt="" />
            </div>
            <video ref={video} id='nutral-video' className='video' controls>
              <source src="about-nutra.mp4" type="video/mp4" />
            </video>

            {/* <ReactPlayer id='nutral-video' ref={video} width='100%' height='100%' className='video' url='about-nutra.mp4' controls muted playsinline autoPlay /> */}

          </div>

        </div>

        <div className='tip2'>
          温馨提示：购买了纽乐养胃粉的客人记得把小票保存好哦！ <br></br>
          欢迎届时参与纽乐微博微信线上抽奖等缤纷活动
        </div>

        <div className='offical-nutra'>
          <img className='nutra-lift-logo' src={process.env.PUBLIC_URL + '/imgs/nutra-life-logo.svg'} alt="nutra life" />
          <div className='t-right h6 black'>
            纽乐健康官方微博号 <br></br>
            @纽乐Nutra-Life
          </div>
        </div>



      </div>
    </div>
  );
}

export default App;
