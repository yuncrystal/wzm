import React, { useRef, useEffect, useState } from 'react';
// import { Player, ControlBar } from 'video-react';
import ReactPlayer from 'react-player'
import './App.css';

function App() {
  const video = useRef(null);
  const videoBackground = useRef(null);
  return (
    <div>
      <div className='pc-banner'>
        <img src={process.env.PUBLIC_URL + '/imgs/pc-banner.png'} alt="pc-banner" />
      </div>
      <div className='app'>
        <div className="container">
          <div className='title padding'>
            <img src={process.env.PUBLIC_URL + '/imgs/title.svg'} alt="Gut Health Powder" />
            <div className='title-content'>纽乐养胃粉</div>

          </div>
          <div className='banner'>
            <img className='banner-img' src={process.env.PUBLIC_URL + '/imgs/banner1.png'} alt="Gut Relif" />

            <div className='banner-group'>
              <div className='row-1'>
                <img className='group-1' src={process.env.PUBLIC_URL + '/imgs/group-1.png'} alt="group-1.png" />
                <img className='group-2' src={process.env.PUBLIC_URL + '/imgs/group-2.png'} alt="group-2.png" />
                <img className='group-3' src={process.env.PUBLIC_URL + '/imgs/group-3.png'} alt="group-3.png" />
              </div>
              <div className='row-2'>
                <img className='group-4' src={process.env.PUBLIC_URL + '/imgs/group-4.png'} alt="group-4.png" />
                <img className='group-5' src={process.env.PUBLIC_URL + '/imgs/group-5.png'} alt="group-5.png" />
              </div>
            </div>
          </div>

          <div className='label-right-container'>
            <div className='label-right'>
              澳洲注册药剂师<br></br>共同倾情推荐
        </div>
          </div>

          <div className='IRI-logo padding'>
            <img src={process.env.PUBLIC_URL + '/imgs/IRI-logo.png'} alt="IRI-logo" />
            <div className='h7 black'>澳洲权威数据统计机构数据所得，纽乐养胃粉全澳销量No.1</div>
          </div>

          <div className='product-effect'>
            <div className='label-left'>
              <div className='label-left-content h3'>产品功效</div>
            </div>
            <div className='effect-list padding'>
              <div className='effect-row'>
                <img src={process.env.PUBLIC_URL + '/imgs/effect-1.svg'} alt="" />
                <div>维护健康的消化功能，保养肝脏。</div>
              </div>
              <div className='effect-row'>
                <img src={process.env.PUBLIC_URL + '/imgs/effect-2.svg'} alt="" />
                <div>缓解消化不良和肠胃气胀，帮助舒缓受到胃肠道 刺激的肠壁黏膜。</div>
              </div>
              <div className='effect-row'>
                <img src={process.env.PUBLIC_URL + '/imgs/effect-3.svg'} alt="" />
                <div>含有益生元，促进肠道内有益菌群健康生长， 缓解便秘。</div>
              </div>
              <div className='effect-row'>
                <img src={process.env.PUBLIC_URL + '/imgs/effect-4.svg'} alt="" />
                <div>好喝的芒果香橙味</div>
              </div>

            </div>

          </div>

          <div className='tips'>
            <div className='label-left-tip'>
              <div className='label-left-content h3'>使用小贴士</div>
            </div>

            <div className='tips-list padding'>
              <div className='tip-row'>
                <div className='orange font20'>01</div>
                <div className='tip-content'>
                  <div className='bold'>建议冷水或温水冲泡，水温不宜过高</div>
                  <div>以保留养胃粉中的益生元活性及营养成分。</div>
                </div>
              </div>

              <div className='tip-row'>
                <div className='orange font20'>02</div>
                <div className='tip-content'>
                  <div className='bold'>内附量勺，每次使用一平勺 </div>
                  <div>一勺6克兑150ml冷水或温水搅均匀即可饮用。</div>
                </div>
              </div>

              <div className='tip-row'>
                <div className='orange font20'>03</div>
                <div className='tip-content'>
                  <div className='bold'>每日一次，或遵医嘱 </div>
                  <div>服用后请大量饮水。</div>
                </div>
              </div>
            </div>
          </div>

          <div className='about-left-tip'>
            <div className='label-left-content h3'>关于纽乐Nutra-Life</div>
          </div>
          <div className='video-container' >
            <div ref={videoBackground} className='video-background' onClick={(e)=>{
                video.current.play();
                video.current.muted = false;
                video.current.style.display = 'block';
                videoBackground.current.style.zIndex = -1;
            }}>
              <img style={{ width: '100%',height: '100%'}} src={process.env.PUBLIC_URL + '/imgs/background.png'} alt="" />
            </div>
            <video ref={video} id='nutral-video' className='video' controls>
              <source src="about-nutra.mp4" type="video/mp4" />
            </video>

            {/* <ReactPlayer id='nutral-video' ref={video} width='100%' height='100%' className='video' url='about-nutra.mp4' controls muted playsinline autoPlay /> */}

          </div>

          <div className='section-mistory padding'>
            <div className='title-who'>Who?</div>
            <div className='mistory-content'>猜一猜谁为纽乐养胃粉强力打Call？<br></br>
              What???还大力推荐给他的粉丝？？？<br></br>
              6.27微博搜索#胃靠七分养 吃货尝天下#<br></br>
              看Vlog参与互动齐打call！！！</div>
          </div>
          <div className='weibo-section padding'>
            <div>
              <img src={process.env.PUBLIC_URL + '/imgs/weibo.png'} alt="weibo-logo" />
            </div>
            <div className='weibo-content'>#胃靠七分养 吃货尝天下#</div>
          </div>
          <div className='weibo-tip padding'>提示：他是顾家男儿，可以跟太太从中午吃到晚上</div>

          <div className='wechart-section'>
            <div className='wechat-logo'>
              <img src={process.env.PUBLIC_URL + '/imgs/wechat.png'} alt="weibo-logo" />
            </div>
            <div className='weibo-content'>
              或扫描活动小助手了解详细
          </div>
            <div className='mistory-img'>
              <img src={process.env.PUBLIC_URL + '/imgs/mistory.png'} alt="mistory" />
            </div>
            <div className='QR'>
              <img src={process.env.PUBLIC_URL + '/imgs/QR.png'} alt="weibo-logo" />
            </div>
          </div>
        </div>

        <div className='tip2'>
          温馨提示：购买了纽乐养胃粉的客人记得把小票保存好哦！ <br></br>
          欢迎届时参与纽乐微博微信线上抽奖等缤纷活动
        </div>

        <div className='offical-nutra'>
          <img className='nutra-lift-logo' src={process.env.PUBLIC_URL + '/imgs/nutra-life-logo.svg'} alt="nutra life" />
          <div className='t-right h6 black'>
            纽乐健康官方微博号 <br></br>
            @纽乐Nutra-Life
          </div>
        </div>



      </div>
    </div>
  );
}

export default App;
